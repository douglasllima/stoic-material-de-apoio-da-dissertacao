


# SToIC - Material de Apoio da Dissertação



Material da revisão sistemática da dissertação intitulada SToIC - Portabilidade e Interoperabilidade na Camada de Aplicação de Plataformas de Internet das Coisas.

## Planilhas e Tabelas

[Clique aqui](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/revisao.ods) para ver o arquivo contendo as planilhas referenciadas no Capítulo 2.

Abaixo, versão online das planilhas e cópia das tabelas relacionadas às plataformas levantas e os recursos encontrados.

### Capítulo 2

- [**Planilha 1:** Lista de estudos secundários selecionados ao término do processo (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela1.md)

- [**Planilha 2:** Período abrangido pelos estudos selecionados (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela2.md)

- [**Planilha 3:** Estudos primários levantados pela revisão sistemática (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela3.md)

- [**Planilha 4:** Artigos adicionados ao trabalho no ano de 2020 (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela4.md)

- [**Tabela 3:** Plataformas de IoT encontradas nos estudos secundários (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela5.md)

- [**Tabela 4:** Plataformas de IoT encontradas nos estudos primários (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela6.md)

- [**Tabela 5:** Tabela comparativa entre as plataformas de IoT quanto aos seus recursos (Fonte: Próprio autor).](https://gitlab.com/douglasllima/stoic-material-de-apoio-da-dissertacao/-/blob/master/tabelas/tabela7.md)
