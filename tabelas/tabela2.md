**Tabela 2:** Período abrangido pelos estudos selecionados (Fonte: Próprio autor).

<table>
<thead>
  <tr>
    <th>Referência</th>
    <th>2010</th>
    <th>2011</th>
    <th>2012</th>
    <th>2013</th>
    <th>2014</th>
    <th>2015</th>
    <th>2016</th>
    <th>2017</th>
    <th>2018</th>
    <th>2019</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td><a href="https://doi.org/10.1145/2633661.2633667" target="_blank" rel="noopener noreferrer">Petrolo, Loscri e Mitton (2014)</a></td>
    <td colspan="10">Não informado</td>
  </tr>
  <tr>
    <td><a href="https://doi.org/10.1109/IC2E.2016.43" target="_blank" rel="noopener noreferrer">Yangui et al. (2016)</a></td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://doi.org/10.1109/MIPRO.2016.7522237" target="_blank" rel="noopener noreferrer">Pflanzner e Kertész (2016)</a></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://doi.org/10.1016/j.fcij.2017.02.001" target="_blank" rel="noopener noreferrer">Ray (2016)</a></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://doi.org/10.1109/COMST.2015.2444095" target="_blank" rel="noopener noreferrer">Al-Fuqaha et al. (2015)</a></td>
    <td colspan="10">Não informado</td>
  </tr>
</tbody>
</table>
