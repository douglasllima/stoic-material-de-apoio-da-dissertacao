**Tabela 9**: *Endpoints* da interface disponibilizada pelo SToIC às aplicações (Fonte: Próprio autor).

<table>
<thead>
  <tr>
    <th>Registros</th>
    <th>Endpoints</th>
    <th>Descrição</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td rowspan="6">Plataformas</td>
    <td>GET /broker</td>
    <td>Lista todas as instâncias de plataformas cadastradas.</td>
  </tr>
  <tr>
    <td>POST /broker</td>
    <td>Insere uma nova instância de plataforma.</td>
  </tr>
  <tr>
    <td>PUT /broker</td>
    <td>Atualiza uma instância de plataforma registrada.</td>
  </tr>
  <tr>
    <td>GET /broker/brokerId</td>
    <td>Recupera o registro de uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>DELETE /broker/brokerId</td>
    <td>Remove o registro de uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>GET /broker/schema/brokerType</td>
    <td>Recupera o esquema dos campos para registrar uma instância de um determinado tipo de plataforma.</td>
  </tr>
  <tr>
    <td rowspan="4">Dispositivos</td>
    <td>GET /device</td>
    <td>Recupera dispositivos registrados nas diferentes instâncias de plataformas.</td>
  </tr>
  <tr>
    <td>POST /device</td>
    <td>Registra um novo dispositivo em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>GET /device/deviceId</td>
    <td>Recupera um dispositivo em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>DELETE /device/deviceId</td>
    <td>Remove um dispositivo de uma instância de plataforma.</td>
  </tr>
  <tr>
    <td rowspan="5">Entidades</td>
    <td>GET /entity</td>
    <td>Recupera entidades de uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>POST /entity</td>
    <td>Registra uma nova entidade em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>PUT /entity</td>
    <td>Atualizar uma entidade em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>GET /entity/entityId</td>
    <td>Recuperar uma entidade em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>DELETE /entity/entityId</td>
    <td>Remove uma entidade em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td rowspan="5">Associações</td>
    <td>GET /association</td>
    <td>Recupera associações entre dispositivos e entidades.</td>
  </tr>
  <tr>
    <td>POST /association</td>
    <td>Registra uma nova associação.</td>
  </tr>
  <tr>
    <td>PUT /association</td>
    <td>Atualiza uma associação.</td>
  </tr>
  <tr>
    <td>GET /association/associationId</td>
    <td>Recupera uma associação.</td>
  </tr>
  <tr>
    <td>DELETE /association/associationId</td>
    <td>Remove uma associação.</td>
  </tr>
  <tr>
    <td rowspan="5">Tarefas</td>
    <td>GET /task</td>
    <td>Recupera todas as tarefas de uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>POST /task</td>
    <td>Cria uma nova tarefa</td>
  </tr>
  <tr>
    <td>GET /task/taskId</td>
    <td>Recupera uma tarefa de uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>PUT /task/taskId</td>
    <td>Atualiza uma tarefa em uma instância de plataforma.</td>
  </tr>
  <tr>
    <td>DELETE /task/taskId</td>
    <td>Remove uma tarefa</td>
  </tr>
</tbody>
</table>
